#!/usr/bin/env python
# -*- coding: utf-8 -*-


import argparse
import re
from os.path import join, exists
from os import makedirs

def get_arguments():
    """ Get arguments: corpus file path and output directory. """
    parser = argparse.ArgumentParser(description="Corpus filepath")
    parser.add_argument("-c", "--corpus", type=argparse.FileType('r'),
                        help="Corpus filepath", metavar="C",
                        dest="corpus_file")
    parser.add_argument("-o", "--output_dir",
                        help="Path where results are stored", metavar="D",
                        dest="output_dir")
    try:
        # Parse arguments
        args = parser.parse_args()
        if not args.output_dir:
            args.output_dir = './out'
        if not exists(args.output_dir):
            makedirs(args.output_dir)
        return args.corpus_file,args.output_dir

    except SystemExit:
        return None, None


def make_strip(corpus_file, output_dir):
    """ Given a corpus file and a path where store the  """
    sentences_file = open(join(output_dir,'sentences.txt'),'w')
    for line in corpus_file:
        if line[0]=='U':
            sentences_file.write(line.split(':')[1])
            sentences_file.write('\n')
        elif line[0].islower():
            (data,filename)=line.split(':')
            filename = re.sub(r'[<>?]','_',filename)
            data_file = open(join(output_dir,filename[:-1]), 'a')
            data_file.write(data+'\n')
            data_file.close()
    sentences_file.close()


if __name__ == "__main__":
    corpus_file, output_dir = get_arguments()
    if corpus_file and output_dir:
        make_strip(corpus_file, output_dir)
